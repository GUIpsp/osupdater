package org.osupdater.deob.cfg;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.analysis.*;
import org.osupdater.deob.cfg.heuristics.Heuristic;
import org.osupdater.deob.cfg.heuristics.BranchDecision;
import org.osupdater.deob.cfg.heuristics.SimpleHeuristic;
import org.osupdater.util.Util;
import org.osupdater.util.expressions.ExpressionTracer;

import java.util.*;

public class CFG {

    private static Heuristic avoidReturn = new SimpleHeuristic() {

        @Override
        public BranchDecision run(Block branch, Block fall) {
            if (branch != null && fall != null) {
                if (branch.contains(BlockAnalyzer.terminalInstructions))
                    return BranchDecision.FALL;
                if (fall.contains(BlockAnalyzer.terminalInstructions))
                    return BranchDecision.BRANCH;
            }
            return BranchDecision.INCONCLUSIVE;
        }
    };

    public static List<AbstractInsnNode> getOrderedInstructions(String owner, final MethodNode mn, Heuristic... heuristics) throws AnalyzerException {
        List<AbstractInsnNode> list = new ArrayList<AbstractInsnNode>();
        List<Block> blocks = getBlocks(owner, mn, heuristics);
        for (Block b : blocks) {
            for (AbstractInsnNode insn : b.instructions) {
                list.add(insn);
            }
        }
        return list;
    }

    private static Set<Integer> getCatchBlockIndices(MethodNode mn) {
        Set<Integer> ret = new HashSet<Integer>();
        ListIterator<AbstractInsnNode> it = mn.instructions.iterator();
        while (it.hasNext()) {
            AbstractInsnNode insn = it.next();
            if (insn.getOpcode() == Opcodes.ATHROW) {
                int length = ExpressionTracer.getExpressionLength(insn);
                int idx = mn.instructions.indexOf(insn) - length + 1;
                ret.add(idx);
            }
        }
        return ret;
    }

    private static void printBlock(Block b, MethodNode mn) {
        if (b == null)
            return;
        System.out.println("Block: " + b.start + " " + b.jumpTo + " " + b.fallTo);
        ListIterator<AbstractInsnNode> it = b.instructions.listIterator();
        while (it.hasNext()) {
            AbstractInsnNode insn = it.next();
            Util.printInstruction(insn, mn);
        }
    }

    private static List<Block> getBlocks(String owner, final MethodNode mn, Heuristic... heuristics) throws AnalyzerException {
        List<Heuristic> hsList = new ArrayList<Heuristic>(Arrays.asList(heuristics));
        // If none of the passed heuristics were applicable to a block,
        // we will default to avoid a return.
        hsList.add(avoidReturn);

        BlockAnalyzer a = new BlockAnalyzer(new BasicInterpreter(), mn, getCatchBlockIndices(mn));
        a.analyze(owner, mn);

        List<Block> blocks = new ArrayList<Block>();
        Block block = a.blocks.get(0);
        Set<Integer> visitedBlocks = new HashSet<Integer>();

        while (block != null) {
            blocks.add(block);
            visitedBlocks.add(block.start);
            int next;
            BranchDecision d = BranchDecision.INCONCLUSIVE;
            for (Heuristic h : hsList) {
                BranchDecision tmp;
                if ((tmp = h.run(block, a.blocks)) != BranchDecision.INCONCLUSIVE) {
                    d = tmp;
                    break;
                }
            }
            if (d != BranchDecision.INCONCLUSIVE) {
                if (d == BranchDecision.BRANCH)
                    next = block.jumpTo;
                else
                    next = block.fallTo;
            } else
                next = block.jumpTo;
            if (visitedBlocks.contains(next)) {
                block = a.blocks.get(block.fallTo);
            } else {
                block = a.blocks.get(next);
            }
        }
        return blocks;
    }
}
