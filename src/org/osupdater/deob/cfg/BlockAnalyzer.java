package org.osupdater.deob.cfg;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.JumpInsnNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.TypeInsnNode;
import org.objectweb.asm.tree.analysis.Analyzer;
import org.objectweb.asm.tree.analysis.BasicValue;
import org.objectweb.asm.tree.analysis.Frame;
import org.objectweb.asm.tree.analysis.Interpreter;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class BlockAnalyzer extends Analyzer {

    public Map<Integer, Block> blocks;

    private MethodNode mn;

    private Block currentBlock;

    private Set<Integer> catchBlockIndices;

    private int nextFall = -1;
    private int prevBlock = -1;

    private boolean naturalCatch = false;

    public static Set<Integer> terminalInstructions;

    static {
        terminalInstructions = new HashSet<Integer>();
        terminalInstructions.add(Opcodes.ATHROW);
        terminalInstructions.add(Opcodes.RETURN);
        terminalInstructions.add(Opcodes.IRETURN);
        terminalInstructions.add(Opcodes.DRETURN);
        terminalInstructions.add(Opcodes.LRETURN);
        terminalInstructions.add(Opcodes.FRETURN);
        terminalInstructions.add(Opcodes.ARETURN);
        terminalInstructions.add(-1);
    }

    public BlockAnalyzer(Interpreter interpreter, MethodNode mn, Set<Integer> catchBlockIndices) {
        super(interpreter);
        this.mn = mn;
        this.catchBlockIndices = catchBlockIndices;
        currentBlock = new Block();
        currentBlock.start = 0;
        blocks = new HashMap<Integer, Block>();
    }

    @Override
    protected Frame newFrame(int nLocals, int nStack) {
        return new Node<BasicValue>(nLocals, nStack);
    }

    @Override
    protected Frame newFrame(Frame src) {
        return new Node(src);
    }

    private boolean checkType(AbstractInsnNode insn, String type) {
        if (insn instanceof TypeInsnNode) {
            TypeInsnNode tin = (TypeInsnNode) insn;
            if (tin.getOpcode() == Opcodes.NEW && tin.desc.equals(type)) {
                return true;
            }
        }
        return false;
    }

    protected void newControlFlowEdge(int src, int dest) {
        Node<BasicValue> n = (Node<BasicValue>) getFrames()[src];
        n.successors.add((Node<BasicValue>) getFrames()[dest]);
        AbstractInsnNode cur = mn.instructions.get(src);
        AbstractInsnNode next = mn.instructions.get(dest);
        if (currentBlock.start == -1)
            currentBlock.start = src;
        if (catchBlockIndices.contains(src)) {
            // We have entered a catch block. Save the current block, restore it after ATHROW.
            prevBlock = currentBlock.start;
            if (prevBlock == src)
                naturalCatch = true;
            blocks.put(currentBlock.start, currentBlock);
            Block b = new Block();
            b.start = src;
            currentBlock = b;
        }
        currentBlock.instructions.add(cur);
        if (terminalInstructions.contains(next.getOpcode())) {
            if (next.getOpcode() == Opcodes.ATHROW) {
                currentBlock.instructions.add(next);
                blocks.put(currentBlock.start, currentBlock);
                if (naturalCatch)
                    currentBlock = new Block();
                else
                    currentBlock = blocks.get(prevBlock);
                prevBlock = -1;
            } else {
                // If we determined the end of block from a return or an ATHROW, add it to the current block's list
                if (next.getOpcode() != -1) {
                    currentBlock.instructions.add(next);
                }
                if (next.getOpcode() == -1) {
                    currentBlock.jumpTo = dest;
                    // If the block ends in a goto, let's check if the target block is just a return.
                    // If so, stick a return after the goto - while the return instruction doesn't really belong there,
                    // it will simplify our processing by a lot when we want to determine which out of two blocks is
                    // a dead end.
                    if (cur.getOpcode() == Opcodes.GOTO) {
                        AbstractInsnNode tmp = mn.instructions.get(dest + 1);
                        if (terminalInstructions.contains(tmp.getOpcode()))
                            currentBlock.instructions.add(tmp);
                    }
                }
                if (nextFall >= 0) {
                    AbstractInsnNode insn = mn.instructions.get(nextFall);
                    if (checkType(insn, "java/lang/IllegalStateException"))
                        currentBlock.fallTo = -1;
                    else
                        currentBlock.fallTo = nextFall;
                }
                blocks.put(currentBlock.start, currentBlock);
                currentBlock = new Block();
                nextFall = -1;
            }
        } else if (cur instanceof JumpInsnNode) {
            nextFall = dest;
        }

    }

}
