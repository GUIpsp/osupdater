package org.osupdater.deob.cfg;

import org.objectweb.asm.tree.AbstractInsnNode;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Block {

    public List<AbstractInsnNode> instructions;

    public int start = -1;
    public int jumpTo = -1;
    public int fallTo = -1;

    public boolean contains(int opcode) {
        for (AbstractInsnNode i : instructions) {
            if (i.getOpcode() == opcode)
                return true;
        }
        return false;
    }

    public boolean contains(Set<Integer> opcodes) {
        for (AbstractInsnNode i : instructions) {
            if (i.getOpcode() != -1)
                if (opcodes.contains(i.getOpcode()))
                    return true;
        }
        return false;
    }

    public Block()
    {
        this.instructions = new ArrayList<AbstractInsnNode>();
    }
}
