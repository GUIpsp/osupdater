package org.osupdater.deob.cfg.heuristics;

public enum BranchDecision {
    BRANCH,
    FALL,
    INCONCLUSIVE
}
