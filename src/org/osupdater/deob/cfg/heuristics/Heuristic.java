package org.osupdater.deob.cfg.heuristics;

import org.osupdater.deob.cfg.Block;

import java.util.Map;

public interface Heuristic {

    // Returning true indicates that we should follow the jump at the end of the block
    public BranchDecision run(Block b, Map<Integer, Block> blocks);

}
