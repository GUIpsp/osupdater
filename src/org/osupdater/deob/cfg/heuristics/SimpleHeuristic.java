package org.osupdater.deob.cfg.heuristics;

import org.osupdater.deob.cfg.Block;

import java.util.Map;

public abstract class SimpleHeuristic implements Heuristic {

    @Override
    public BranchDecision run(Block b, Map<Integer, Block> blocks) {
        return run(blocks.get(b.jumpTo), blocks.get(b.fallTo));
    }

    public abstract BranchDecision run(Block branch, Block fall);

}
