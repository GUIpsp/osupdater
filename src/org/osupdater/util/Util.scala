package org.osupdater.util

import java.math.BigInteger

import org.objectweb.asm.Opcodes
import org.objectweb.asm.tree._
import org.objectweb.asm.util.Printer
import org.osupdater.deob.cfg.{Block, CFG}
import org.osupdater.deob.cfg.heuristics.Heuristic

// Helper functions galore
object Util {

  // Used for inverting integers
  val modulus = new BigInteger(String.valueOf(1L << 32))
  val sint = new BigInteger(String.valueOf(1L << 31))

  def matchIntOperand(a: AbstractInsnNode, operand: Int): Boolean = {
    a match {
      case (i: IntInsnNode) => operand == i.operand
      case _ => false
    }
  }

  // Find an instruction with the opcode in a method
  def findInstruction(cur: Option[AbstractInsnNode], check: Any): Option[AbstractInsnNode] = {
    cur match {
      case Some(insn) =>
        check match {
          case opcode: Int => insn.getOpcode match {
            case `opcode` => cur
            case _ => findInstruction(Option(insn.getNext), opcode)
          }
          case f: (AbstractInsnNode => Boolean) =>
            if (f(insn))
              Option(insn)
            else
              findInstruction(Option(insn.getNext), f)
        }
      case None => None
    }
  }

  // See if an instruction with the opcode exists
  def existsInstruction(method: MethodNode, check: Any): Boolean = {
    findInstruction(Option(method.instructions.getFirst), check).isDefined
  }

  // Get the first instruction matching the opcode
  def getInstruction(method: MethodNode, check: Any): Option[AbstractInsnNode] = {
    findInstruction(Option(method.instructions.getFirst), check)
  }

  // Check if the method throws an exception
  def checkException(method: MethodNode): Boolean = {
    existsInstruction(method, Opcodes.ATHROW)
  }

  // Convert the Java List of MethodNodes to a Scala List
  def listMethods(classNode: ClassNode): List[MethodNode] = {
    List(classNode.methods.toArray(new Array[MethodNode](0)) : _*)
  }

  // Convert the Java List of FieldNodes to a Scala List
  def listFields(classNode: ClassNode): List[FieldNode] = {
    List(classNode.fields.toArray(new Array[FieldNode](0)) : _*)
  }

  // Convert the Java List of a Block to a Scala List
  def listInstructions(b: Block): List[AbstractInsnNode] = {
    List(b.instructions.toArray(new Array[AbstractInsnNode](0)) : _*)
  }

  // Convert a MethodNode to a list of instructions, no sorting
  def listInstructions(mn: MethodNode): List[AbstractInsnNode] = {
    var list = List[AbstractInsnNode]()
    var i = mn.instructions.getLast
    while (i != null) {
      list = i :: list
      i = i.getPrevious
    }
    list
  }

  // Convert the InsnList of a MethodNode to a Scala list, sorting blocks
  def listInstructions(mn: MethodNode, owner: String, h: Heuristic*): List[AbstractInsnNode] = {
    List(CFG.getOrderedInstructions(owner, mn, h : _*).toArray(new Array[AbstractInsnNode](0)) : _*)
  }

  // Take the modular inverse of a value
  def reverseMultiplier(ldc: String): String = {
    val reverse = new BigInteger(ldc).modInverse(modulus)
    // if the multiplier is larger than the max value of a signed integer,
    // let's truncate it down so that we don't have to do any special handling
    // when parsing our JSON in the bot.
    if (reverse.compareTo(sint) == 1) {
      reverse.mod(sint).toString
    } else
      reverse.toString
  }

  // Find a pattern of opcodes in a list of instructions
  def findPattern(instructions: List[AbstractInsnNode], pattern: List[Any]): Boolean = {
    instructions.exists((p) => matchPattern(p, pattern))
  }

  // Find a pattern of opcodes in a list of instructions
  def getPattern(cur: AbstractInsnNode, pattern: List[Any], acc: List[AbstractInsnNode]): List[AbstractInsnNode] = {
    pattern match {
      case hd :: tl =>
        if (matchInstruction(cur, hd))
         getPattern(cur.getNext, tl, cur :: acc)
        else
          Nil
      case _ => acc.reverse
    }
  }

  def getPattern(list: List[AbstractInsnNode], pattern: List[Any]): List[AbstractInsnNode] = {
    list match {
      case hd :: tl =>
        getPattern(hd, pattern, Nil) match {
          case Nil => getPattern(tl, pattern)
          case x => x
        }
    }
  }

  def matchInstruction(insn: AbstractInsnNode, p: Any): Boolean = {
    Option(insn).isDefined && (
    p match {
      case opcode: Int => opcode == insn.getOpcode
      case func: (AbstractInsnNode => Boolean) => func(insn)
    })
  }

  // Match a pattern of opcodes starting from an instruction
  def matchPattern(cur: AbstractInsnNode, pattern: List[Any]): Boolean = {
    pattern match {
      case hd :: tl => matchInstruction(cur, hd) && matchPattern(cur.getNext, tl)
      case _ => true
    }
  }

  def instructionToString(insn: AbstractInsnNode, mn: MethodNode): String = {
    mn.instructions.indexOf(insn) + ": " + instructionToString(insn)
  }

  def instructionToString(insn: AbstractInsnNode): String = {
    insn match {
      case (i: FieldInsnNode) => Printer.OPCODES(i.getOpcode) + " " + i.owner + "/" +i.name + " " + i.desc
      case (i: LdcInsnNode) => Printer.OPCODES(i.getOpcode) + " " + i.cst
      case (i: VarInsnNode) => Printer.OPCODES(i.getOpcode) + " " + i.`var`
      case (i: IntInsnNode) => Printer.OPCODES(i.getOpcode) + " " + i.operand
      case (i: MethodInsnNode) => Printer.OPCODES(i.getOpcode) + " " + i.owner + "/" + i.name + " " + i.desc
      case (i: TypeInsnNode) => Printer.OPCODES(i.getOpcode) + " " + i.desc
      case x if x.getOpcode != -1 => Printer.OPCODES(x.getOpcode)
      case x => "LABEL"
    }
  }

  def printInstruction(insn: AbstractInsnNode, mn: MethodNode): Unit = {
    println(instructionToString(insn, mn))
  }

  def printInstruction(insn: AbstractInsnNode): Unit = {
    println(instructionToString(insn))
  }

  // Determine how the constant must be adjusted to make the
  // jump instruction take the jump
  def adjustParam(jmp: JumpInsnNode, ldc: LdcInsnNode): String = {
    jmp.getOpcode match {
      case Opcodes.IF_ICMPEQ => ldc.cst.toString
      case Opcodes.IF_ICMPGE => ldc.cst.toString
      case Opcodes.IF_ICMPLE => ldc.cst.toString
      case Opcodes.IF_ICMPGT => (ldc.cst.toString.toInt + 1).toString
      case Opcodes.IF_ICMPLT => (ldc.cst.toString.toInt - 1).toString
      case Opcodes.IF_ICMPNE => (ldc.cst.toString.toInt - 1).toString
    }
  }

  // Get the junk value that has to be passed to a game function in order not to
  // trigger an IllegalStateException.
  def getExceptionParam(instructions: List[AbstractInsnNode], varNum: Int): String = {
    instructions match {
      case (a: VarInsnNode) :: (b: LdcInsnNode) :: (c: JumpInsnNode) :: tl if a.`var` == varNum => adjustParam(c, b)
      case hd :: tl => getExceptionParam(tl, varNum)
      case _ => "ERR"
    }
  }



}
