package org.osupdater.util.expressions

import org.objectweb.asm.{Opcodes, Type}
import org.objectweb.asm.tree.{MethodInsnNode, AbstractInsnNode}
import org.osupdater.util.Util

object ExpressionTracer {

  // Traces all the instructions that a given instruction takes as an argument, and returns the list of instructions
  // that forms the complete expression. For example, if ran on an IMUL, it'll return the list of instructions that
  // forms the factors, with the IMUL at the end.
  def trace(insn: Option[AbstractInsnNode], accumulator: List[AbstractInsnNode], stack: Int): List[AbstractInsnNode] = {
    insn match {
      case Some(i) =>
        if (i.getOpcode == -1) // label
          trace(Option(i.getPrevious), accumulator, stack)
        else {
          var newStack = 0
          i match {
            // Get the number of arguments a method takes = the minimum number of new instructions needed.
            // If the call is not INVOKESTATIC, (atleast) one more instruction is required for the object reference.
            case (call: MethodInsnNode) =>
              newStack = stack + Type.getArgumentTypes(call.desc).length
              if (call.getOpcode == Opcodes.INVOKESTATIC)
                newStack -= 1
              if (call.desc.endsWith("V") && accumulator.size > 0)
                newStack += 1
              // The exception object reference will already be on the stack when an exception occurs
              // So when they go to pass it to their internal exception handler, there isn't an instruction
              // above the method call to produce the reference.
              if (call.desc.contains("Ljava/lang/Throwable;"))
                newStack -= 1
            // Not a method call, fixed number of args, look it up in the table
            case _ =>
              newStack = stack + Args.instructions(i.getOpcode) - 1
          }
          // System.out.print(newStack + " after ");Util.printInstruction(i);
          // Everything balances out!

          if (newStack == 0)
            i :: accumulator
          else
            trace(Option(i.getPrevious), i :: accumulator, newStack)
        }
      case None =>
        accumulator
    }
  }

  def trace(insn: AbstractInsnNode): List[AbstractInsnNode] = {
    trace(Option(insn), List(), 1)
  }

  def getExpressionLength(insn: AbstractInsnNode): Int = {
    trace(insn).size
  }

}
