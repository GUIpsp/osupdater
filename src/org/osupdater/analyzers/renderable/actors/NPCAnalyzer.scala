package org.osupdater.analyzers.renderable.actors

import org.osupdater.util.Util
import org.osupdater.{Updater, Analyzer}

class NPCAnalyzer (name: String)
  extends Analyzer (name) {

  def run: Boolean = {
    classNode.superName == Updater.classMap("Actor") &&
    Util.listFields(classNode).exists((p) => p.desc == "L" + Updater.classMap("NPCDefinition") + ";" && add("definition", List(p.desc, p.name)))
  }
}
