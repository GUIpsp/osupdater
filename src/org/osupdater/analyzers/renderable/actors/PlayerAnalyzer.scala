package org.osupdater.analyzers.renderable.actors

import org.osupdater.util.Util
import org.osupdater.{Updater, Analyzer}

class PlayerAnalyzer (name: String)
  extends Analyzer (name) {

  def run: Boolean = {
    classNode.superName == Updater.classMap("Actor") &&
      Util.listFields(classNode).exists((p) =>
        p.desc == "L" + Updater.classMap("PlayerDefinition") + ";" && add("definition", List(p.desc, p.name))) &&
      Util.listFields(classNode).exists((p) => p.desc == "Ljava/lang/String;" && add("name", List(p.desc, p.name)))
  }

}
