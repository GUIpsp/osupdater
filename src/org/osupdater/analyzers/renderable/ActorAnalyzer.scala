package org.osupdater.analyzers.renderable

import org.objectweb.asm.Opcodes
import org.objectweb.asm.tree._
import org.osupdater.util.Util
import org.osupdater.util.expressions.ExpressionTracer
import org.osupdater.{Analyzer, Updater}

class ActorAnalyzer(name: String)
  extends Analyzer(name) {

  // See if this is actually the Actor class based on the walking queues that are initialized to int[10] in the ctor
  def identifyClass: Boolean = {
    classNode.superName == Updater.classMap("Renderable") &&
      methods
        .filter((m) => m.name == "<init>")
        .exists((m) => Util.listInstructions(m, className)
          .count((insn) => insn.getOpcode == Opcodes.BIPUSH && insn.asInstanceOf[IntInsnNode].operand == 10) == 3)
  }

  // Get the field for the current text spoken by the entity
  def getActorMessage: Boolean = {
    Util.listFields(classNode).exists((f) =>
      f.desc == "Ljava/lang/String;" && {
        add("message", List(f.desc, f.name))
      }
    )
  }

  // From the -1 check in the beginning of "setPosition",
  def getAnimation(instructions: List[AbstractInsnNode]): Boolean = {
    val name = instructions.find((p) => p.getOpcode == Opcodes.GETFIELD).get.asInstanceOf[FieldInsnNode].name
    val multiplier = instructions.find((p) => p.getOpcode == Opcodes.LDC).get.asInstanceOf[LdcInsnNode].cst.toString
    add("animation", List("I", name, multiplier))
  }

  def checkMultiplication(name: String, field: String, insn: AbstractInsnNode): Boolean = {
    val expr = ExpressionTracer.trace(insn)
    expr.exists{case (f: FieldInsnNode) => f.desc == "[I" case _ => false} && {
      expr.find((l) => l.getOpcode == Opcodes.LDC) match {
        case Some(ldc: LdcInsnNode) =>
          add(name, List("I", field, Util.reverseMultiplier((ldc.cst.toString.toInt / 128).toString)))
        case _ => false
      }
    }
  }

  /*
    From the end of "setPosition",

    this.h = 806404544 * this.d + -1970968448 * this.br[0];
    this.k = -1710346944 * this.d + 1519708544 * this.by[0];

    We want to grab h and k (local x and y) along with their multipliers.
    The multiplier is the one from the array look up div 128 (the deob is just 128 * arr[0]).
  */
  def getPosition(instructions: List[AbstractInsnNode], name: String): Boolean = {
    instructions match {
      case (put: FieldInsnNode) :: (add: InsnNode) :: (mul: InsnNode) :: tail =>
        put.getOpcode == Opcodes.PUTFIELD && add.getOpcode == Opcodes.IADD && mul.getOpcode == Opcodes.IMUL && {
          val addition = ExpressionTracer.trace(add)
          addition.filter((m) => m.getOpcode == Opcodes.IMUL) match {
            case x if x.size == 2 => x.exists((p) => checkMultiplication(name, put.name, p)) && {
              name match {
                case "localY" => getPosition(tail, "localX")
                case "localX" => true
              }
            }
            case _ => getPosition(tail, name)
          }
        }
      case hd :: tail => getPosition(tail, name)
      case _ => false
    }
  }

  def run: Boolean = {
    identifyClass && getActorMessage &&
    methods.exists(
      (m) => m.desc.matches("\\(IIZ.\\)V") &&
        Util.checkException(m) && {
        // Position and animation are grabbed from the same method
        val list = Util.listInstructions(m, className)
        getAnimation(list) && getPosition(list.reverse, "localY")
      }
    )
  }

}
