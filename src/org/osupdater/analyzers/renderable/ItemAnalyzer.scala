package org.osupdater.analyzers.renderable

import java.lang.reflect.Modifier

import org.objectweb.asm.Opcodes
import org.objectweb.asm.tree.{LdcInsnNode, FieldInsnNode, MethodNode}
import org.osupdater.util.Util
import org.osupdater.util.expressions.ExpressionTracer
import org.osupdater.{Updater, Analyzer}

/**
 * Created by Juuso on 4/6/2015.
 */
class ItemAnalyzer (name: String)
  extends Analyzer(name) {

  def grabVars(m: MethodNode): Boolean = {
    Util.checkException(m) && !Modifier.isStatic(m.access) && m.desc.matches("\\(.\\)L.*") && {
      val list = Util.listInstructions(m)
      list.filter((p) => p.getOpcode == Opcodes.IMUL) match {
        case a :: b :: tl =>
          val expr = ExpressionTracer.trace(b)
          // At this point we are in the right method for sure,
          // and the below instructions _will_ be found, so no checking for Defined/None
          val ldc = expr.find((p) => p.getOpcode == Opcodes.LDC)
          val fld = expr.find((p) => p.getOpcode == Opcodes.GETFIELD)
          add("stack", List("I", fld.get.asInstanceOf[FieldInsnNode].name, ldc.get.asInstanceOf[LdcInsnNode].cst.toString))
        case _ => false
      }
    }
  }

  def run: Boolean = {
    classNode.superName == Updater.classMap("Renderable") &&
    methods.count((m) => m.name == "<init>") == 1 &&
    methods.exists((m) => m.name == "<init>" && Util.listInstructions(m)(3).getOpcode == Opcodes.RETURN) &&
    methods.exists((m) => grabVars(m))
  }
}
