package org.osupdater.analyzers.renderable

import org.objectweb.asm.Opcodes
import org.objectweb.asm.tree._
import org.osupdater.util.Util
import org.osupdater.util.expressions.ExpressionTracer
import org.osupdater.{Analyzer, Updater}

class RenderableAnalyzer(name: String)
  extends Analyzer (name){

  def checkCode(instructions: List[AbstractInsnNode], varNum: Int): Boolean = {
    instructions match {
      case cur :: tail => cur.getOpcode match {
        case Opcodes.ILOAD =>
          // Look for the block of ILOADs 1 through 9
          cur.asInstanceOf[VarInsnNode].`var` == varNum && (varNum > 8 || checkCode(tail, varNum + 1))
        case _ => (varNum == 1) && checkCode(tail, varNum) // Can't have a gap in the ILOADs
      }
      case _ => false
    }
  }

  // Renderable is a subclass of CacheNode. We'll attempt to identify it based on the "renderAtPoint" method.
  def run: Boolean = {
    classNode.superName == Updater.classMap("CacheNode") &&
      methods.exists(
        (m) => m.desc == "(IIIIIIIII)V" && Util.checkException(m) && checkCode(Util.listInstructions(m, className), 1) && {
          // If we are in the right method, the only putfield is for model height.
          val f = Util.listInstructions(m, className).find((p: AbstractInsnNode) => p.getOpcode == Opcodes.PUTFIELD && p.asInstanceOf[FieldInsnNode].desc == "I").get.asInstanceOf[FieldInsnNode]
          //val f = Util.getInstruction(m, (p: AbstractInsnNode) => p.getOpcode == Opcodes.PUTFIELD && p.asInstanceOf[FieldInsnNode].desc == "I").get.asInstanceOf[FieldInsnNode]
          val ldc = ExpressionTracer.trace(f).find((p) =>p.getOpcode == Opcodes.LDC)
          var cst = "1"
          if (ldc.isDefined)
            cst = Util.reverseMultiplier(ldc.get.asInstanceOf[LdcInsnNode].cst.toString)
          add("modelHeight", List(f.desc, f.name, cst))
        }
      )
  }
}