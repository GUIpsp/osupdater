package org.osupdater.analyzers.node

import org.objectweb.asm.Opcodes
import org.objectweb.asm.tree._
import org.osupdater.util.Util
import org.osupdater.{Analyzer, Updater}

// This class is a subclass of Node and has two fields.
// next is easily identifiable from a null check in the unlink method (which is the only non-ctor method),
// and the remaining one must be prev.
class CacheNodeAnalyzer(name: String)
  extends Analyzer (name) {

  def checkUnlink(instructions: List[AbstractInsnNode]): Boolean = {
    val Next: Option[Any] = results.get("next")
    instructions match {
      case insn :: tail => insn.getOpcode match {
        case Opcodes.GETFIELD =>
          val fin = insn.asInstanceOf[FieldInsnNode]
          results.size match {
          case 0 =>
            add("next", List(fin.desc, fin.name))
          case 1 => Option(List(fin.desc, fin.name)) match {
            case Next =>
            case n => return add("prev", n.get)
          }
        }
        case _ =>
      }
      checkUnlink(tail)
      case _ => false
    }
  }

  def checkMethods(methods: List[MethodNode]): Boolean = {
    methods match {
      case hd :: tail => hd.name match {
        case "<init>" => checkMethods(tail)
        case _ => checkUnlink(Util.listInstructions(hd, className))
      }
    }
  }

  def run(desc: String): Boolean = {
    val fields = List(classNode.fields.toArray(new Array[FieldNode](0)) : _*)
    classNode.superName == Updater.classMap("Node") && fields.size == 2 &&
      fields(0).desc == desc && fields(1).desc == desc && checkMethods(methods)
  }

  def run: Boolean = {
    run("L" + className + ";")
  }

}
