package org.osupdater.analyzers.definitions

import org.objectweb.asm.Opcodes
import org.objectweb.asm.tree._
import org.osupdater.{Updater, Analyzer}
import org.osupdater.util.Util

class ItemDefAnalyzer (name: String)
  extends Analyzer (name) {

  def getGroundActions(list: List[AbstractInsnNode]): Boolean = {
    list match {
      case (a: FieldInsnNode) :: (b: FieldInsnNode) :: tl
        if a.desc == "Ljava/lang/String;" && b.desc == "[Ljava/lang/String;" =>
          add("groundActions", List(b.desc, b.name))
      case hd :: tl => getGroundActions(tl)
      case _ => false
    }
  }

  def getInventoryActions(list: List[AbstractInsnNode]): Boolean = {
    list match {
      case (a: InsnNode) :: (b: FieldInsnNode) :: tl
        if a.getOpcode == Opcodes.ACONST_NULL && b.desc == "[Ljava/lang/String;" =>
          add("inventoryActions", List(b.desc, b.name))
      case hd :: tl => getInventoryActions(tl)
      case _ => false
    }
  }

  def run: Boolean = {
    classNode.superName == Updater.classMap("CacheNode") &&
    methods.filter((m) => m.name == "<init>").exists(
      (m) => {
        val l = Util.listInstructions(m, className)
        getInventoryActions(l.filterNot((p) => p.getOpcode == Opcodes.AASTORE)) &&
          getGroundActions(l.filterNot((p) => p.getOpcode == Opcodes.AASTORE))
      }
    ) &&
    Util.listFields(classNode).exists((f) => f.desc == "Ljava/lang/String;" && add("name", List(f.desc, f.name)))
  }

}
