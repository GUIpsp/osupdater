package org.osupdater.analyzers.definitions

import java.lang.reflect.Modifier
import java.util

import org.objectweb.asm.{Type, Opcodes}
import org.objectweb.asm.tree._
import org.osupdater.deob.cfg._
import org.osupdater.deob.cfg.heuristics.{Heuristic, BranchDecision}
import org.osupdater.util.Util
import org.osupdater.util.expressions.ExpressionTracer
import org.osupdater.{Updater, Analyzer}

import scala.annotation.tailrec

class ObjectDefAnalyzer (name: String)
  extends Analyzer (name) {

  val guardHeuristic = new Heuristic {

    def run(list: List[AbstractInsnNode]): BranchDecision = {
      list match {
        case (iload: VarInsnNode) :: (ifeq : JumpInsnNode) :: tl
          if (iload.`var` == 5 || iload.`var` == 6) && ifeq.getOpcode == Opcodes.IFEQ => BranchDecision.FALL
        case hd :: tl => run(tl)
        case _ => BranchDecision.INCONCLUSIVE
      }
    }

    override def run(b: Block, blocks: util.Map[Integer, Block]): BranchDecision = {
      run(List(b.instructions.toArray(new Array[AbstractInsnNode](0)) : _*))
    }
  }

  // Identify the class based on a "<< 10", which the other definition classes don't have.
  def checkShift(instructions: List[AbstractInsnNode]): Boolean = {
    instructions match {
      case (x: AbstractInsnNode) :: (i: IntInsnNode) :: (shl: InsnNode) :: tl
        if x.getOpcode != Opcodes.IALOAD && i.operand == 10 && shl.getOpcode == Opcodes.ISHL => true
      case hd :: tl => checkShift(tl)
      case _ => false
    }
  }

  /*
    Grab model size and offset xyz from this:

    if(needsScaling)
      model_3.scale(modelSizeX, modelSizeH, modelSizeY);
    if(modelIsOffset)
      model_3.translate(offsetX, offsetH, offsetY);

    guardHeuristic is passed to the deobber to make sure both method calls are visited, and in this order.
  */
  def checkMethodCalls(instructions: List[AbstractInsnNode], names: List[String]): Boolean = {
    instructions match {
      case (call: MethodInsnNode) :: tail if call.getOpcode == Opcodes.INVOKEVIRTUAL && call.desc == "(III)V" =>
        val expr = ExpressionTracer.trace(call)
        // each this.x * cst is 4 instructions, and we have 3 of them. Then the call itself and the obj ref.
        if (expr.length == 14) {
          // For every multiplication in the list, pull the getfield and ldc
          var vars = Map[String, String]()
          expr.filter((i) => i.getOpcode == Opcodes.IMUL).foreach((i) => {
            val mul = ExpressionTracer.trace(i)
            (mul.find((p) => p.getOpcode == Opcodes.LDC), mul.find((p) => p.getOpcode == Opcodes.GETFIELD)) match {
              case (Some(ldc: LdcInsnNode), Some(get: FieldInsnNode)) => vars += (get.name -> ldc.cst.toString)
            }
          })
          (names, vars).zipped.foreach{case (a, (b, c)) => add(a, List("I", b, c))}
        }
        results.size match {
          case 8 => true
          case 5 => checkMethodCalls(tail, List("offsetX", "offsetY", "offsetZ"))
          case _ => checkMethodCalls(tail, names)
        }
      case hd :: tl => checkMethodCalls(tl, names)
      case _ => false
    }
  }

  def getChildDef(m: MethodNode): Boolean = {
    Modifier.isPublic(m.access) &&
      !Modifier.isStatic(m.access) &&
      m.desc.endsWith(")L" + className + ";") &&
      Type.getArgumentTypes(m.desc).length == 1 && {
      add("getChildDefinition", List(m.desc, m.name, Util.getExceptionParam(Util.listInstructions(m, className), 1)))
    }
  }

  def run: Boolean = {
    classNode.superName == Updater.classMap("CacheNode") &&
    Util.listFields(classNode).exists((f) => f.desc == "[Ljava/lang/String;" && add("actions", List(f.desc, f.name))) &&
    Util.listFields(classNode).exists((f) => f.desc == "Ljava/lang/String;" && add("name", List(f.desc, f.name))) &&
    methods.exists((m) => Util.listInstructions(m, className).exists((p) => p.getOpcode == Opcodes.ISHL &&
      {
        p.getPrevious match {
          case (ten: IntInsnNode) => ten.operand == 10
          case _ => false
        }
      }
    )) &&
      methods.exists((m) =>
      m.desc.matches("[(][I][I][A-Z][)][L]..[;]") &&
        checkMethodCalls(
          Util.listInstructions(m, className, guardHeuristic), List("modelSizeX", "modelSizeY", "modelSizeZ"))) &&
      methods.exists((m) => getChildDef(m))
  }

}
