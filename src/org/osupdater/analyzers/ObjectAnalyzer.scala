package org.osupdater.analyzers

import org.objectweb.asm.Opcodes
import org.objectweb.asm.tree.{MethodNode, LdcInsnNode, FieldInsnNode, VarInsnNode}
import org.osupdater.util.Util
import org.osupdater.util.expressions.ExpressionTracer
import org.osupdater.{Updater, Analyzer}

class ObjectAnalyzer(name: String)
  extends Analyzer(name) {

  val idxUID = 12
  var idxCFG = 13

  def grabLVAssignment(m: MethodNode, name: String, num: Int): Boolean = {
    Util.listInstructions(m, className).exists((p) =>
      p.getOpcode == Opcodes.PUTFIELD && {
        val expr = ExpressionTracer.trace(p)
        className = p.asInstanceOf[FieldInsnNode].owner
        expr.exists((i) => i.getOpcode == Opcodes.ILOAD && i.asInstanceOf[VarInsnNode].`var` == num) &&
          add(name, List("I", p.asInstanceOf[FieldInsnNode].name, Util.reverseMultiplier(expr.find((l) =>
            l.getOpcode == Opcodes.LDC).get.asInstanceOf[LdcInsnNode].cst.toString)))
      }
    )
  }

  def run: Boolean = {
    // Identify the interactable object class by
    // the object creation method in Region
    className == Updater.classMap("Region") &&
      methods.exists((m) =>
        m.desc.matches("\\(IIIIIIIIL..;IZI.\\)Z") &&
          grabLVAssignment(m, "uid", idxUID) &&
          grabLVAssignment(m, "cfg", idxCFG)
      )
  }

}
