package org.osupdater.analyzers.client

import java.lang.reflect.Modifier

import org.objectweb.asm.Opcodes._
import org.objectweb.asm.tree._
import org.objectweb.asm.util.Printer
import org.osupdater.Updater
import org.osupdater.deob.cfg.Block
import org.osupdater.deob.cfg.heuristics.{BranchDecision, SimpleHeuristic}
import org.osupdater.util.{ByteLocations, Util}
import org.osupdater.util.expressions.ExpressionTracer

object StaticAnalyzers {

  // [name, className.fieldName]
  var results = Map[String, Any]()

  // [name, function]
  var analyzers = scala.collection.mutable.Map[String, ClassNode => Boolean]()

  addAnalyzer("canvas", (c: ClassNode) => Util.listFields(c).exists((f) =>
    f.desc == "Ljava/awt/Canvas;" && add("canvas", List(c.name, f.desc, f.name))))

  addAnalyzer("localPlayer", (c: ClassNode) => Util.listFields(c).exists((f) =>
    f.desc == "L" + Updater.classMap("Player") + ";" && add("localPlayer", List(c.name, f.desc, f.name))) )

  addAnalyzer("players", (c: ClassNode) => Util.listFields(c).exists((f) =>
    f.desc == "[L" + Updater.classMap("Player") + ";" && add("players", List(c.name, f.desc, f.name))) )

  addAnalyzer("npcs", (c: ClassNode) => Util.listFields(c).exists((f) =>
    f.desc == "[L" + Updater.classMap("NPC") + ";" && add("npcs", List(c.name, f.desc, f.name))) )

  // Base coordinates --------------------------------------------------------------------------------------------------

  def getBaseCoordinate(get: String, name: String, l: List[AbstractInsnNode]): Boolean = {
    l.exists((p) => p.getOpcode == ISHR && Util.matchIntOperand(p.getPrevious, 7) && {
      ExpressionTracer.trace(p).exists {case (f: FieldInsnNode) => f.name == get case _=> false} && {
        Util.findInstruction(Option(p), ISTORE) match {
          case Some(i) =>
            val mul = ExpressionTracer.trace(i)
            val gs = mul.find((z) => z.getOpcode == GETSTATIC && z.asInstanceOf[FieldInsnNode].desc == "I")
            gs.isDefined && {
              val f = gs.get.asInstanceOf[FieldInsnNode]
              add(name, List(f.owner, "I", f.name, mul.find((p) => p.getOpcode == LDC).get.asInstanceOf[LdcInsnNode].cst.toString))
            }
          case _ => false
      }}
    })
  }

  addAnalyzer("base", (c: ClassNode) => Util.listMethods(c).exists((m) => Util.checkException(m) && {
    val list = Util.listInstructions(m)
    getBaseCoordinate(Updater.results("Actor.localX"), "baseX", list) &&
    getBaseCoordinate(Updater.results("Actor.localY"), "baseY", list)
    })
  )

  // Cam variables -----------------------------------------------------------------------------------------------------

  def getCamVars(i: AbstractInsnNode): Boolean = {
    i match {
      case m: MethodInsnNode if m.getOpcode == INVOKEVIRTUAL && m.owner == Updater.classMap("Region") && m.desc == "(IIIIII)V" =>
        val expr = ExpressionTracer.trace(i)
        expr.size == 18 && {
          (List("camX", "camZ", "camY", "camPitch", "camYaw"),
            expr.tail.filter((p) => p.getOpcode == GETSTATIC),
              expr.filter((p) => p.getOpcode == LDC)).zipped.toList.forall {
            case (a, b: FieldInsnNode, c: LdcInsnNode) =>
              add(a, List(b.owner, b.desc, b.name, c.cst.toString))
          }
        }
      case _ => false
    }
  }

  addAnalyzer("cam", (c: ClassNode) => Util.listMethods(c).exists((m) =>{
    Util.listInstructions(m).exists((p) => getCamVars(p))}
  ))

  // Map variables -----------------------------------------------------------------------------------------------------

  def getMapVar(name: String, cst: String, a: AbstractInsnNode): Boolean = {
    a match {
      case (f: FieldInsnNode) if f.desc == "I" && f.getOpcode == PUTSTATIC =>
        val expr = ExpressionTracer.trace(a)
        expr.exists{case (min: MethodInsnNode) => min.name == "random" case _ => false} && {
          expr.exists{case (l: LdcInsnNode) => l.cst.toString == cst case _ => false}  &&
            add(name, List(f.owner, f.desc, f.name, expr.find{
              case (z: LdcInsnNode) => !z.cst.toString.contains(".") case _ => false
            }.get.asInstanceOf[LdcInsnNode].cst.toString))
        }
      case _ => false
    }
  }

  addAnalyzer("map", (c: ClassNode) => Util.listMethods(c).exists((m) => Util.checkException(m) && {
    val list = Util.listInstructions(m)
    list.exists((p) => getMapVar("mapScale", "20.0", p)) &&
      list.exists((p) => getMapVar("mapAngle", "120.0", p)) &&
      list.exists((p) => getMapVar("mapOffset", "30.0", p))
  }))

  //--------------------------------------------------------------------------------------------------------------------

  addAnalyzer("tileData", (c: ClassNode) => Util.listMethods(c).exists((m) => m.desc.matches("\\(L..;IIIIII.\\)V") &&
    Modifier.isStatic(m.access) &&
    Util.listInstructions(m).exists{case (p: FieldInsnNode) => p.desc == "[[[I" && add("tileHeights", List(p.owner, p.desc, p.name)) case _ => false} &&
    Util.listInstructions(m).exists((p) => p.getOpcode == BASTORE && {
      val expr = ExpressionTracer.trace(p)
      expr.exists((p) => p.getOpcode == ICONST_0) && (expr.find((p) => p.getOpcode == GETSTATIC) match {
        case Some(f: FieldInsnNode) if f.desc == "[[[B" => add("tileFlags", List(f.owner, f.desc, f.name))
        case None => false
      })
    }))
  )

  // Grab plane from where it is used as an index for tile flags
  addAnalyzer("plane", (c: ClassNode) => Util.listMethods(c).exists((m) => m.desc.startsWith("([L") &&
    Util.listInstructions(m).exists((p) => p.getOpcode == AALOAD && {
      val expr = ExpressionTracer.trace(p)
      expr.exists { case (f: FieldInsnNode) if f.desc == "[[[B" => true case _ => false} &&
        (expr.find { case (f: FieldInsnNode) if f.desc == "I" => true case _ => false} match {
          case Some(f: FieldInsnNode) => add("plane", List(f.owner, "I", f.name, expr.find((p) => p.getOpcode == LDC).get.asInstanceOf[LdcInsnNode].cst.toString ))
          case _ => false
        })
    })
  ))

  // Grab menu action/option arrays from the method that concatenates them
  addAnalyzer("menuStrings", (c: ClassNode) => Util.listMethods(c).exists((m) => Modifier.isStatic(m.access) &&
    m.desc.matches("\\(I.\\)Ljava/lang/String;") && Util.checkException(m) && {
      val list = Util.listInstructions(m).filter((p) => p.getOpcode == GETSTATIC && p.asInstanceOf[FieldInsnNode].desc == "[Ljava/lang/String;"  && p.getPrevious != null && p.getPrevious.getOpcode > 0)
      list.size > 0 && list.forall {
        case (p: FieldInsnNode) =>
          p.getPrevious.getOpcode match {
            case INVOKESPECIAL => add("menuActions", List(p.owner, "[Ljava/lang/String;", p.name))
            case INVOKEVIRTUAL => add("menuOptions", List(p.owner, "[Ljava/lang/String;", p.name))
          }
      }
    }
  ))

  // Grab the menu location vars where they are passed to a method of desc (IIII.)V
  addAnalyzer("menuLocation", (c: ClassNode) => Util.listMethods(c).exists((m) => m.desc.matches("\\(.\\)V") && Util.checkException(m) && {
      val list = Util.listInstructions(m)
      list.exists {
        case (min: MethodInsnNode) if min.getOpcode == INVOKESTATIC && min.desc.matches("\\(IIII.\\)V") =>
          val expr = ExpressionTracer.trace(min)
          expr.size == 14 && {
            (List("menuX", "menuY", "menuWidth", "menuHeight"),
              expr.filter((p) => p.getOpcode == GETSTATIC),
              expr.filter((p) => p.getOpcode == LDC)).zipped.toList.forall {
              case (a, b: FieldInsnNode, c: LdcInsnNode) =>
                add(a, List(b.owner, b.desc, b.name, c.cst.toString))
            }
          }
        case _ => false
      } && (Util.getPattern(list, List(IMUL, ICONST_1, ISUB)) match {
        case a :: b :: sub :: tl =>
          val expr = ExpressionTracer.trace(sub)
          (expr.find((p) => p.getOpcode == GETSTATIC), expr.find((p) => p.getOpcode == LDC)) match {
            case (Some(f: FieldInsnNode), Some(l: LdcInsnNode)) => add("menuCount", List(f.owner, f.desc, f.name, l.cst.toString))
          }
      })
    }
  ))

  val itemCreation = "{\"class\": \"%s\", \"name\": \"%s\", \"sig\": \"%s\", \"byte\": %d, \"id\": 5, \"x\": 2, \"y\": 3, \"stack\": 4}"
  val itemRestack = "{\"class\": \"%s\", \"name\": \"%s\", \"sig\": \"%s\", \"byte\": %d, \"id\": 4, \"x\": 2, \"y\": 3, \"stack\": 6}"
  val itemRemoval = "{\"class\": \"%s\", \"name\": \"%s\", \"sig\": \"%s\", \"byte\": %d, \"id\": 1, \"x\": 3, \"y\": 4}"

  // The packet parsing method is used below to identify the following breakpoint locations:
  // - Ground item spawn
  // - Ground item stack resize (if two stackable items are dropped on the same tile, the game server merges them)
  // - Ground item remove
  addAnalyzer("packetParser", (c: ClassNode) => Util.listMethods(c).exists((m) => m.desc.matches("\\(.\\)V") && Modifier.isStatic(m.access)
    && Util.checkException(m) && {
    val list = Util.listInstructions(m)
    (list.count { case (p: IntInsnNode) if p.getOpcode == BIPUSH && p.operand == 7 => true case _ => false} > 10) && {
      list.find{case (p: TypeInsnNode) => p.getOpcode == NEW && p.desc == Updater.classMap("Item") case _ => false} match {
        case Some(i) => add("itemSpawn", String.format(itemCreation, c.name, m.name, m.desc, ByteLocations.getInstructionByteIndex(i, c.name): Integer))
        case _ => false
      }
    } && {
      // Bwuahahaha, "Queue.unlink()" is the only void method that is called in the packet parser method,
      // saves me from identifying the queue class just for this :) Might have to do it for something else, though.
      list.find{case (m: MethodInsnNode) => m.desc == "()V" && m.getPrevious.getOpcode == ALOAD case _ => false} match {
        case Some(i) => add("itemRemove", String.format(itemRemoval, c.name, m.name, m.desc, ByteLocations.getInstructionByteIndex(i, c.name): Integer))
        case _ => false
      }
    } && {
      list.find{
        case (f: FieldInsnNode) => f.getOpcode == PUTFIELD && f.owner == Updater.classMap("Item") && f.name == Updater.results("Item.stack")
        case _ => false
      } match {
        case Some(i) => add("itemRestack", String.format(itemRemoval, c.name, m.name, m.desc, ByteLocations.getInstructionByteIndex(i, c.name): Integer))
        case _ => false
      }
    }
  }
  ))

  def addAnalyzer(name: String, func: ClassNode => Boolean): Unit = {
    analyzers += (name -> func)
  }

  def isFinished: Boolean = {
    analyzers.size == 0
  }

  def runAll(classNode: ClassNode): Unit = {
    for ((name, analyzer) <- analyzers)
        if (analyzers(name)(classNode))
          analyzers -= name
  }

  def printFailures: Unit = {
    println("Failed statics:")
    for ((name, analyzer) <- analyzers)
      println(name)
  }

  def add(name: String, data: Any): Boolean = {
    results += (name -> data)
    true
  }

}
