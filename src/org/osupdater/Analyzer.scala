package org.osupdater

import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.MethodNode
import org.osupdater.util.Util

abstract class Analyzer (val name: String) {
  var results:Map[String, Any] = Map()
  var className: String = ""
  var classNode: ClassNode = null
  var methods: List[MethodNode] = List()
  def run: Boolean

  // Saves us from writing true in awkward places
  def add(name: String, data: Any): Boolean = {
    results += (name -> data)
    true
  }

  def checkMethods(methods: List[MethodNode], func:((MethodNode) => Boolean)*): Boolean = {
    methods.exists((p) => func.forall((f) => f(p)))
  }

  def analyze(classNode: ClassNode): Boolean = {
    className = classNode.name
    this.classNode = classNode
    results = Map() // Clear for every run, we only want to keep the vars from the right class
    methods = Util.listMethods(classNode)
    run
  }

}
