package org.osupdater

import java.util.jar.{JarEntry, JarFile}

import org.objectweb.asm.{ClassReader}
import org.objectweb.asm.tree.{ClassNode}
import org.osupdater.analyzers.{ObjectAnalyzer, RegionAnalyzer}
import org.osupdater.analyzers.client.StaticAnalyzers
import org.osupdater.analyzers.definitions.{PlayerDefAnalyzer, ItemDefAnalyzer, ObjectDefAnalyzer, NPCDefAnalyzer}
import org.osupdater.analyzers.input.RSMouseAnalyzer
import org.osupdater.analyzers.node.{CacheNodeAnalyzer, NodeAnalyzer}
import org.osupdater.analyzers.renderable.actors.{PlayerAnalyzer, NPCAnalyzer}
import org.osupdater.analyzers.renderable.{ItemAnalyzer, ActorAnalyzer, RenderableAnalyzer, ModelAnalyzer}

import scala.sys.process.{ProcessLogger, Process}
import scala.util.control.Breaks._

object Updater {

  // Maps class names to ClassNodes
  var classes = Map[String, ClassNode]()

  // Maps deobfuscated names to obfuscated names
  var classMap = Map[String, String]()

  // Result data made accessible to other analyzers
  var results = Map[String, String]()

  var classReaders = Map[String, ClassReader]()

  // Read a class file from the jar
  def parseClasses(enumeration: java.util.Enumeration[JarEntry], jar: JarFile): Unit = {
    if (enumeration.hasMoreElements) {
      val entry = enumeration.nextElement()
      if (entry.getName.endsWith(".class")) {
        val classNode = new ClassNode()
        val cr = new ClassReader(jar.getInputStream(entry))
        cr.accept(classNode, ClassReader.SKIP_DEBUG | ClassReader.SKIP_FRAMES)
        classReaders += (classNode.name -> cr)
        classes += (classNode.name -> classNode)
      }
      parseClasses(enumeration, jar)
    }
  }

  // Parse class files from the jar
  def parseClasses(path: String): Unit = {
    val jar = new JarFile(path)
    val enumeration = jar.entries()
    parseClasses(enumeration, jar)
  }

  // Generate JSON object for a result set
  def resultsToJSON(results: Map[String, Any]): String = {
    results.map{
      case (name, field) =>
        // check if JSON is taken care of in the analyzer
        field match {
          case s: String => "\"" + name + "\": " + field
          case l: List[String] =>
            "\"" + name + "\": [" + l.map((d) => "\"" + d + "\"").mkString(", ") + "]"
        }
    }.mkString(",\n")
  }

  // Runs an analyzer on the entire game
  def runAnalyzer(analyzer: Analyzer): String = {
    for ((k, v) <- classes) {
      if (analyzer.analyze(v)) {
        val str = "\n\"" + analyzer.name + "\": {\n\"class\": \"" + analyzer.className + "\",\n" + resultsToJSON(analyzer.results) + "\n}"
        classMap += (analyzer.name -> k)
        for ((k, v) <- analyzer.results) {
          v match {
            case l: List[String] => results += ((analyzer.name + "." + k) -> l(1).toString)
            case _ =>
          }
        }
        return str
      }
    }
    // We absolutely cannot continue if something breaks, it means maintenance of the updater is required, period.
    // While it might at first seem okay to let a few hooks slip and give the bot partial functionality,
    // this is a bad idea as a script might hit a corner case which ends up killing the character or causing a ban
    // due to a malfunction caused by the missing hook(s).
    throw new RuntimeException("Analyzer: " + analyzer.name + " is broken! Abort mission!")
  }

  def runAnalyzers(analyzers: List[Analyzer]): String = {
    analyzers.map((a) => runAnalyzer(a)).mkString(",\n")
  }

  // Many of the static field analyzers require information from the class analyzers,
  // so we'll have to make another pass through the game classes, rather than doing it above.
  def runStaticAnalyzers(): String = {
    breakable {
      for ((k, v) <- classes) {
        if (StaticAnalyzers.isFinished)
          break()
        StaticAnalyzers.runAll(v)
      }
    }
    // To stick with the format
    resultsToJSON(StaticAnalyzers.results)
  }

  def main(args: Array[String]): Unit = {
   // val time = System.currentTimeMillis();
    if (args.length == 0) {
      println("Usage: Updater <jarpath>")
      return
    }

    //val command = "javap -classpath " + args(0) +  " -c af";
    //val proc = Process(command).run(ProcessLogger((l) => (), error => println("Error: " + error)))

    parseClasses(args(0))
    // Analyzers are prepended to a list in order of dependence (except for the independent ones),
    // i.e. Node has to be found before CacheNode,
    // so the list must be reversed at the end
    val analyzers = List(
      new NodeAnalyzer("Node"),
      new CacheNodeAnalyzer("CacheNode"),
      new RenderableAnalyzer("Renderable"),
      new ModelAnalyzer("Model"),
      new ActorAnalyzer("Actor"),
      new NPCDefAnalyzer("NPCDefinition"),
      new ObjectDefAnalyzer("ObjectDefinition"),
      new ItemDefAnalyzer("ItemDefinition"),
      new PlayerDefAnalyzer("PlayerDefinition"),
      new NPCAnalyzer("NPC"),
      new PlayerAnalyzer("Player"),
      new RegionAnalyzer("Region"),
      new ObjectAnalyzer("Object"),
      new ItemAnalyzer("Item"),
      new RSMouseAnalyzer("RSMouse")
    )
    // The JSON structure is manually generated, as the structure of our output is not subject to change,
    // and is only three levels deep. (Firing up a lib like gson doesn't seem to be worth it.)
    val reg = runAnalyzers(analyzers)
    val stat = runStaticAnalyzers()
    val json = "{" + reg + ",\n\n\"Client\": {\n" + stat + "\n}\n}"
    println(json)
    StaticAnalyzers.printFailures
  }

}
